import { Plugins } from '@capacitor/core'

const { Network } = Plugins

export async function connectionState () {
  return await Network.getStatus().then(response => {
    if (response.connected) {
      return true
    }
  })
}
