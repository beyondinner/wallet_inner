export default {
  email: 'Email',
  password: 'Password',
  signIn: 'Get IN',
  signUp: 'Sign up',
  forgetPassword: 'Forgot your password?',
  noConnection: 'You don\'t have internet',
  noConnectionDesc: 'BeinAPP only works when you are connected to the internet',
  testConnection: 'Test connection',
  dateLastUpdate: 'last update',
  availableBalance: 'Available Innercoins',
  operations: 'Operations',
  transfer: {
    label: 'Transfer',
    description: 'Transfer your Innercoins to other partners without limits'
  },
  charge: {
    label: 'Buy Innercoins',
    description: 'Top up your account to continue shopping'
  },
  petitions: 'Petitions',
  myBalance: {
    label: 'Check my balance',
    description: 'Check the balance of your account'
  },
  lastOperations: {
    label: 'Last Operations',
    description: 'Last 20 operations performed'
  },
  products: 'Products',
  productsList: {
    label: 'Products',
    description: 'List of available products'
  },
  servicesList: {
    label: 'Services',
    description: 'List of available services'
  },
  data: 'Interesting data',
  community: {
    label: 'Who we are?',
    description: 'Beyond inner global movement',
    content: 'Join the movement of thousands of people from all over the world who have decided to take the step towards their own creative power After 7 years in which we have been creating the foundations and barely warming up engines as a human and business organization, we are going to start a new era of greater global expansion as a result of a natural flowering of the original idea and of the people that make up this human and business group, which we affectionately call FAMILY. Now we take a step further, we create the first club in the world of internal and internal evolution, from today.'
  },
  schoolBeyond: {
    label: 'BeyondInner School',
    description: 'Beyond inner global movement',
    content: 'Join the movement of thousands of people from all over the world who have decided to take the step towards their own creative power After 7 years in which we have been creating the foundations and barely warming up engines as a human and business organization, we are going to start a new era of greater global expansion as a result of a natural flowering of the original idea and of the people that make up this human and business group, which we affectionately call FAMILY. Now we take a step further, we create the first club in the world of internal and internal evolution, from today.'
  },
  aboutFounder: {
    label: 'About the founder',
    description: 'When creativity becomes transgressive',
    content: 'It was essential to innovate in the world of self-improvement and personal development. Consciousness, spirituality and psychotherapy needed a transformation. An enormous contribution to this has been the creative deployment of ALVERTO between 2012 and 2019, a period in which methods, techniques, approaches and training systems were born that are being used by people from the 5 continents in more than 500 retreats a year. ' +
      '<br>' +
      '<br>' +
      'ALVERTO has gone beyond words, and offers a whole transformation engineering in which a technology is used that is reaching very deep and to the hearts of people who are open to inner evolution. These 7 years of creative, intellectual and conceptual development, with thousands of articles published on the blog and hundreds of hours of videos available in the Virtual Classroom. "' +
      '<br>' +
      '<br>' +
      'The schools founded by Alverto have attracted and trained a great international team of more than 100 talented and creative people from dozens of countries, who support the movement and the club.'
  },
  copyRight: {
    label: '© Copyright '+new Date().getFullYear(),
    description: 'The first international club of internal evolution with internal economy'
  },
  price: 'Price',
  buyProduct: 'Buy product',
  noMoney: 'You do not have enough balance',
  noMoneyDescription: 'Your balance is not enough to make the purchase, you can buy more balance from the application itself',
  haveMoney: 'Congratulations, the purchase has been made correctly.',
  buyInfo: 'Purchase information',
  productName: 'Product name',
  discount: 'Discounted balance',
  buyServices: 'Buy services',
  getOut: {
    label: 'Close Session',
    description: 'Close the session started in the system'
  },
  memberEmail: 'Club member\'s email to send',
  balanceToSend: 'Balance to send to member',
  sendBalance: 'To transfer',
  reciveBalance: 'Recive balance',
  balanceToBuy: 'Balance to buy',
  buy: 'Buy balance',
  buyBalance: 'Buy Innercoins',
  buyBalanceDescription: 'Innercoins can only be used to buy BeyondInner products.',
  buyBalanceDescriptionField: '** Enter the amount of euros you want to convert into Innercoins',
  transferDescriptionField: '** Enter the amount of Innercoins you want send',
  transferFailNoMoney: 'You do not have enough money to make the transfer',
  transferFailNoMember: 'There is no member',
  transferFailNoMemberDescription: 'There is no member with the written email, please check the data provided',
  transferMoney: 'Congratulations, the transfer has been made correctly.',
  buyBalanceDescriptionPaypal: 'Select the option you want to carry out the process',
  buyProductTag: 'Buy Product',
  buyServiceTag: 'Buy Service',
  amount: 'Amount',
  date: 'Date',

  registerLabel: {
    title: 'Become a partner today',
    subtitle: 'Don\'t waste any more time'
  },

  register: {
    fullName: 'Full name',
    birthDay: 'Birthday',
    phone: 'Phone number',
    email: 'Email',
    passportID: 'Passport / ID',
    address: 'Address',
    country: 'Country',
    adviser: 'Adviser'
  },

  okRegisterText: 'THANK YOU FOR REGISTERING, AN ADVISOR WILL CONTACT YOU SHORTLY',
  existEmail: 'Sorry this email is taken, please use another one.'
}
