export default {
  email: 'Correo',
  password: 'Contraseña',
  signIn: 'Entrar',
  signUp: 'Crear cuenta',
  forgetPassword: '¿Olvido su contraseña?',
  noConnection: 'No dispones de internet',
  noConnectionDesc: 'BeinAPP solo funciona cuando estas conectado a internet',
  testConnection: 'Probar conexión',
  dateLastUpdate: 'ultima actualización',
  availableBalance: 'Saldo en Innercoins',
  operations: 'Operaciones',
  transfer: {
    label: 'Transferencia',
    description: 'Transfiere tus Innercoins a otros socios sin límites'
  },
  charge: {
    label: 'Comprar Innercoins',
    description: 'Recarga tu cuenta para seguir comprando'
  },
  petitions: 'Peticiones',
  myBalance: {
    label: 'Consultar mi saldo',
    description: 'Consultar el saldo de su cuenta'
  },
  lastOperations: {
    label: 'Ultimas operaciones',
    description: 'Ultimas 20 operaciones realizadas'
  },
  products: 'Productos',
  productsList: {
    label: 'Productos',
    description: 'Listado de productos disponibles'
  },
  servicesList: {
    label: 'Servicios',
    description: 'Listado de productos services'
  },
  data: 'Datos interesantes',
  community: {
    label: 'Quienes somos?',
    description: 'Beyond inner movimiento global',
    content: 'Únete al movimiento de miles de personas de todo el mundo que han decidido dar el paso hacia su propio poder creador Después de 7 años en los que hemos estado creando las bases y apenas calentando motores como organización humana y empresarial, vamos a iniciar una nueva era de mayor expansión mundial como resultado de un florecimiento natural de la idea original y de las personas que conformamos este grupo humano y empresarial, que afectivamente llamamos FAMILIA. Ahora damos un paso más allá, creamos el primer club del mundo de lo interno y la evolución interior, desde hoy.'
  },
  schoolBeyond: {
    label: 'Escuela BeyondInner',
    description: 'Beyond inner global movement',
    content: 'Join the movement of thousands of people from all over the world who have decided to take the step towards their own creative power After 7 years in which we have been creating the foundations and barely warming up engines as a human and business organization, we are going to start a new era of greater global expansion as a result of a natural flowering of the original idea and of the people that make up this human and business group, which we affectionately call FAMILY. Now we take a step further, we create the first club in the world of internal and internal evolution, from today.'
  },
  aboutFounder: {
    label: 'Sobre el fundador',
    description: 'Cuando la creatividad se vuelve transgresora',
    content: 'Era indispensable innovar en el mundo de la superación y desarrollo personal. La consciencia, la espiritualidad y la psicoterapéutica necesitaban una transformación. Una enorme aportación a ello a sido el despliegue creativo de ALVERTO entre el 2012 y 2019, período en el que nacieron métodos, técnicas, abordajes y sistemas formativos que están siendo utilizados por personas de los 5 continentes en más de 500 retiros al año.' +
      '<br>' +
      '<br>' +
      'ALVERTO ha ido más allá de las palabras, y ofrece toda una ingeniería de transformación en la que se utiliza una tecnología que está llegando muy profundo y al corazón de personas que se abren a la evolución interior. Estos 7 años de desarrollo creativo, intelectual y conceptual, con miles de artículos publicados en el blog y cientos de horas de vídeos disponibles en el Aula Virtual.“' +
      '<br>' +
      '<br>' +
      'Las escuelas fundadas por Alverto han atraído y formado a un gran equipo internacional de más de 100 personas talentosas y creativas de decenas de países, que sostienen el movimiento y el club.'
  },
  copyRight: {
    label: '© Copyright '+new Date().getFullYear(),
    description: 'El primer club internacional de evolución interior con economía interna'
  },
  price: 'Precio',
  buyProduct: 'Comprar producto',
  noMoney: 'Usted no tiene suficiente saldo',
  noMoneyDescription: 'Tu saldo no es suficiente para realizar la compra, puedes comprar más saldo desde la propia aplicación',
  haveMoney: 'Enhorabuena, la compra se ha realizado correctamente.',
  buyInfo: 'Informacion de compra',
  productName: 'Nombre del producto',
  discount: 'Saldo descontado',
  buyServices: 'Comprar servicio',
  getOut: {
    label: 'Cerrar sessión',
    description: 'Cerrar la sesión iniciada en el sistema'
  },
  memberEmail: 'Correo electrónico del miembro del club para enviar',
  balanceToSend: 'Saldo para enviar al miembro',
  sendBalance: 'Transferir',
  reciveBalance: 'Recivir saldo',
  balanceToBuy: 'Saldo para comprar',
  buy: 'Comprar saldo',
  buyBalance: 'Compra Innercoins',
  buyBalanceDescription: 'Innercoins solo se pueden usar para comprar productos BeyondInner.',
  buyBalanceDescriptionField: '** Ingrese la cantidad de euros que desea convertir en Innercoins',
  transferDescriptionField: '** Introduce la cantidad de Innercoins que quieres enviar',
  transferFailNoMoney: 'No tienes suficiente dinero para realizar la transferencia',
  transferFailNoMember: 'No hay miembro',
  transferFailNoMemberDescription: 'No hay ningún miembro con el correo electrónico escrito, verifique los datos proporcionados',
  transferMoney: 'Felicitaciones, la transferencia se ha realizado correctamente.',
  buyBalanceDescriptionPaypal: 'Seleccione la opción con la que desea realizar el proceso',
  buyProductTag: 'Compra de producto ',
  buyServiceTag: 'Compra de servicio',
  amount: 'Cantidad',
  date: 'Fecha',

  registerLabel: {
    title: 'Hazte socio hoy mismo',
    subtitle: 'No pierdas mas tiempo'
  },

  register: {
    fullName: 'Nombre completo',
    birthDay: 'Fecha de cumpleaños',
    phone: 'Número de telefono',
    email: 'Correo',
    passportID: 'Pasaporte / ID',
    address: 'Dirección',
    country: 'País',
    adviser: 'Asesor'
  },

  okRegisterText: 'GRACIAS POR REGISTRARTE, EN BREVE UN ASESOR SE PONDRÁ EN CONTACTO CONTIGO.',
  existEmail: 'Lo siento el correo ya esta en uso, por favor use otro.'
}
