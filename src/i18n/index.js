import enUS from './en-us'
import esES from './es-es'
import frFR from './fr-fr'

export default {
  'en-us': enUS,
  'es-es': esES,
  'fr-fr': frFR
}
