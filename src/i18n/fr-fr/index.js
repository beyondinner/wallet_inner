export default {
  email: 'Courrier',
  password: 'Mot de passe',
  signIn: 'Entrer',
  signUp: 'Créer un compte',
  forgetPassword: 'Vous avez oublié votre mot de passe?',
  noConnection: 'Tu n\'as pas internet',
  noConnectionDesc: 'BeinAPP ne fonctionne que lorsque vous êtes connecté à Internet',
  testConnection: 'Tester la connexion',
  dateLastUpdate: 'dernière mise à jour',
  availableBalance: 'Solde Innercoins',
  operations: 'les opérations',
  transfer: {
    label: 'Transfert',
    description: 'Transférez vos Innercoins à d\'autres partenaires sans limites'
  },
  charge: {
    label: 'Acheter du Innercoins',
    description: 'Rechargez votre compte pour continuer vos achats'
  },
  petitions: 'Pétitions',
  myBalance: {
    label: 'Vérifier mon solde',
    description: 'vérifier le solde de votre compte'
  },
  lastOperations: {
    label: 'Dernières opérations',
    description: '20 dernières opérations effectuées'
  },
  products: 'Produits',
  productsList: {
    label: 'Produits',
    description: 'Liste des produits disponibles'
  },
  servicesList: {
    label: 'Prestations de service',
    description: 'Liste des service disponibles'
  },
  data: 'Dates intéressantes',
  community: {
    label: 'Qui sommes nous?',
    description: 'Beyond inner mouvement mondial',
    content: 'Rejoignez le mouvement de milliers de personnes du monde entier qui ont décidé de faire le pas vers leur propre puissance créatrice Après 7 ans au cours desquels nous avons créé les fondations et réchauffé à peine les moteurs en tant qu\'organisation humaine et commerciale, nous allons commencer une nouvelle ère d\'une plus grande expansion mondiale grâce à l\'épanouissement naturel de l\'idée originale et des personnes qui composent ce groupe humain et commercial, que nous appelons affectueusement FAMILLE. Maintenant, nous allons plus loin, nous créons le premier club au monde de l\'évolution interne et interne, à partir d\'aujourd\'hui.'
  },
  schoolBeyond: {
    label: 'École BeyondInner',
    description: 'Beyond inner global movement',
    content: 'Join the movement of thousands of people from all over the world who have decided to take the step towards their own creative power After 7 years in which we have been creating the foundations and barely warming up engines as a human and business organization, we are going to start a new era of greater global expansion as a result of a natural flowering of the original idea and of the people that make up this human and business group, which we affectionately call FAMILY. Now we take a step further, we create the first club in the world of internal and internal evolution, from today.'
  },
  aboutFounder: {
    label: 'À propos du fondateur',
    description: 'Quand la créativité devient transgressive',
    content: 'Il était essentiel d\'innover dans le monde de l\'auto-amélioration et du développement personnel. La conscience, la spiritualité et la psychothérapie avaient besoin d\'une transformation. Une énorme contribution à cela a été le déploiement créatif d\'ALVERTO entre 2012 et 2019, une période au cours de laquelle sont nées méthodes, techniques, approches et systèmes de formation qui sont utilisés par des personnes des 5 continents dans plus de 500 retraites par an.' +
      '<br>' +
      '<br>' +
      'ALVERTO est allé au-delà des mots et propose toute une ingénierie de transformation dans laquelle une technologie est utilisée qui atteint très profondément et dans le cœur des personnes ouvertes à l\'évolution intérieure. Ces 7 années de développement créatif, intellectuel et conceptuel, avec des milliers d\'articles publiés sur le blog et des centaines d\'heures de vidéos disponibles dans la classe virtuelle."' +
      '<br>' +
      '<br>' +
      'Les écoles fondées par Alverto ont attiré et formé une grande équipe internationale de plus de 100 personnes talentueuses et créatives de dizaines de pays, qui soutiennent le mouvement et le club.'
  },
  copyRight: {
    label: '© Copyright '+new Date().getFullYear(),
    description: 'Le premier club international d\'évolution interne avec économie interne'
  },
  price: 'Prix',
  buyProduct: 'Acheter un produit',
  noMoney: 'Vous n\'avez pas assez d\'équilibre',
  noMoneyDescription: 'Votre solde n\'est pas suffisant pour effectuer l\'achat, vous pouvez acheter plus de solde depuis l\'application elle-même',
  haveMoney: 'Félicitations, l\'achat a réussi.',
  buyInfo: 'Informations d\'achat',
  productName: 'Nom du produit',
  discount: 'Solde actualisé',
  buyServices: 'Acheter un service',
  getOut: {
    label: 'Fermer la session',
    description: 'Déconnectez-vous du système'
  },
  memberEmail: 'Email du membre du club à envoyer',
  balanceToSend: 'Solde à envoyer au membre',
  sendBalance: 'Transférer',
  reciveBalance: 'Recevoir  le solde',
  balanceToBuy: 'Solde à acheter',
  buy: 'Acheter du crédit',
  buyBalance: 'Acheter Innercoins',
  buyBalanceDescription: 'Les Innercoins ne peuvent être utilisés que pour acheter des produits BeyondInner.',
  buyBalanceDescriptionField: '** Entrez le montant en euros que vous souhaitez convertir en Innercoins',
  transferDescriptionField: '** Entrez le montant en Innercoins que vous souhaitez envoyer',
  transferFailNoMoney: 'Vous n\'avez pas assez d\'argent pour effectuer le transfert',
  transferFailNoMember: 'Aucun membre',
  transferFailNoMemberDescription: 'Il n\'y a pas de membre avec l\'e-mail écrit, vérifiez les données fournies',
  transferMoney: 'Félicitations, le transfert a réussi.',
  buyBalanceDescriptionPaypal: 'Sélectionnez l\'option avec laquelle vous souhaitez effectuer le processus',
  buyProductTag: 'Achat de produits ',
  buyServiceTag: 'Achat de service ',
  amount: 'Quantité',
  date: 'Date',

  registerLabel: {
    title: 'Devenir un partenaire',
    subtitle: 'Ne perds plus de temps'
  },

  register: {
    fullName: 'Nom complet',
    birthDay: 'Date d\'anniversaire\n',
    phone: 'Numéro de téléphone',
    email: 'Courrier',
    passportID: 'Passeport / ID',
    address: 'Adresse',
    country: 'Pays',
    adviser: 'Conseiller'
  },

  okRegisterText: 'MERCI DE VOUS INSCRIPTION, UN CONSEILLER VOUS CONTACTERA SOUS TENSION',
  existEmail: 'Désolé, l\'e-mail est déjà utilisé, veuillez en utiliser un autre.'
}
