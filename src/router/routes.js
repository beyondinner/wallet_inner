const routes = [
  {
    path: '/',
    name: 'login',
    component: () => import('pages/Login')
  },
  {
    path: '/register',
    name: 'register',
    component: () => import('pages/register/Register')
  },
  {
    path: '',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '/index',
        name: 'index',
        component: () => import('pages/Index.vue')
      },
      {
        path: '/interesting/history',
        name: 'history',
        component: () => import('pages/interesting/History')
      },
      {
        path: '/interesting/beyondschool',
        name: 'beyondSchool',
        component: () => import('pages/interesting/BeyondSchool')
      },
      {
        path: '/interesting/about-founder',
        name: 'aboutFounder',
        component: () => import('pages/interesting/AboutFounder')
      },
      {
        path: '/products/products',
        name: 'products',
        component: () => import('pages/products/Products')
      },
      {
        path: '/products/services',
        name: 'services',
        component: () => import('pages/products/Services')
      },
      {
        path: '/petitions/last-operations',
        name: 'last_operations',
        component: () => import('pages/petitions/LastOperations')
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/errors/Error404.vue')
  }
]

export default routes
